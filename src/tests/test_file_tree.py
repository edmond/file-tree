import re
from file_tree import FileTree, Template, parse_tree
from pathlib import Path
import pytest
from unittest import mock

import file_tree


directory = Path(__file__).parent.joinpath("test_trees")


def test_template_interface():
    """Adds and renames some templates (with or without conflicts)
    """
    tree = FileTree.empty()

    assert tree.top_level == '.'
    assert FileTree.empty(return_path=True).top_level == Path('.')
    tree.add_template("directory")
    assert tree.get_template("directory").as_path == Path("directory")
    tree.add_template("file1.txt")
    assert tree.get_template("file1").as_path == Path("file1.txt")

    with pytest.raises(ValueError):
        tree.add_template("file1.doc")

    with pytest.raises(ValueError):
        tree.add_template("file1.txt", parent="directory")

    tree.add_template("file2.txt", parent="directory")
    assert tree.get_template("file2").as_path == Path("directory/file2.txt")

    tree.add_template("file1.txt", key="other_file1", parent="directory")
    assert tree.get_template("other_file1").as_path == Path("directory/file1.txt")

    with pytest.raises(ValueError):
        tree.add_template("file2.txt")


def test_overwrite_templates():
    """Overwrite existing templates
    """
    tree = FileTree.read(directory / "base.tree", subject="A", top_level="directory")

    assert tree.get("top_file") == "directory/top_file.txt"
    tree.add_template("new_top_file.txt", key="top_file", parent=None, overwrite=True)
    assert tree.get("top_file") == "new_top_file.txt"
    tree.add_template("new_top_file.txt", key="top_file", overwrite=True)
    assert tree.get("top_file") == "directory/new_top_file.txt"

    # overwriting a parent template changes the child
    assert tree.get("nested_dir") == "directory/A/nested_dir"
    assert tree.get("deep_file") == "directory/A/nested_dir/deep_file.txt"
    tree.add_template("new_dir", key="nested_dir", overwrite=True)
    assert tree.get("nested_dir") == "directory/new_dir"
    assert tree.get("deep_file") == "directory/new_dir/deep_file.txt"


def test_variable_interface():
    tree = FileTree.read(directory.joinpath("parent.tree"))
    with pytest.raises(KeyError):
        assert tree.placeholders['session']
    with pytest.raises(KeyError):
        assert tree.placeholders['other_var']
    assert tree.placeholders['base1/session'] == '01'
    with pytest.raises(KeyError):
        assert tree.placeholders['base1/other_var']
    assert tree.placeholders['base2/session'] == '02'
    assert tree.placeholders['base2/other_var'] == 'foo'
    assert tree.placeholders['base3/session'] == '03'

    tree2 = tree.update(inplace=True, session="03")
    assert tree is tree2
    assert tree.placeholders['session'] == '03'
    assert tree.placeholders['base1/session'] == '01'

    tree2 = tree.update(inplace=False, session="05")
    assert tree is not tree2
    assert tree.placeholders['session'] == '03'
    assert tree.placeholders['base1/session'] == '01'
    assert tree2.placeholders['session'] == '05'
    assert tree2.placeholders['base1/session'] == '01'

    tree.update(inplace=True, subject="A")
    assert tree.placeholders['session'] == '03'
    assert tree.placeholders['base1/session'] == '01'
    assert tree.placeholders['subject'] == 'A'
    assert tree.placeholders['base1/subject'] == 'A'


def test_template_keys():
    tree = FileTree.read(directory.joinpath("base.tree"))
    assert len(tree.template_keys()) == 8
    assert sorted(tree.template_keys()) == sorted([
        '', '{subject}', 'file1', 'file2', 'nested_dir', 
        'deep_file', 'int_file', 'top_file',
    ])

    assert len(tree.template_keys(only_leaves=True)) == 5
    assert sorted(tree.template_keys(only_leaves=True)) == sorted([
        'file1', 'file2', 'deep_file', 'int_file', 'top_file',
    ])



def test_linked_placeholders():
    tree = FileTree.read(directory / "parent.tree")
    vars = (["A", "B", "C"], [1, 2, 3])
    tree.placeholders[("subject", "session")] = vars
    assert tree.placeholders['subject'] == vars[0]
    assert tree.placeholders['session'] == vars[1]
    for idx, sub_tree in enumerate(tree.iter_vars(["subject", "session"])):
        assert sub_tree.placeholders['subject'] == vars[0][idx]
        assert sub_tree.placeholders['session'] == vars[1][idx]
    for idx, sub_tree in enumerate(tree.iter_vars(["subject"])):
        assert sub_tree.placeholders['subject'] == vars[0][idx]
        assert sub_tree.placeholders['session'] == vars[1][idx]

    with pytest.raises(ValueError):
        tree.placeholders['subject'] = vars[0]

    with pytest.raises(KeyError):
        del tree.placeholders['subject']

    del tree.placeholders[("session", "subject")]
    tree.placeholders['subject'] = vars[0]
    tree.placeholders['session'] = vars[1]
    assert len(list(tree.iter_vars(["subject", "session"]))) == 9

    tree.placeholders.link('subject', 'session')
    assert len(list(tree.iter_vars(["subject", "session"]))) == 3
    for idx, sub_tree in enumerate(tree.iter_vars(["subject", "session"])):
        assert sub_tree.placeholders['subject'] == vars[0][idx]
        assert sub_tree.placeholders['session'] == vars[1][idx]

    # test IO
    new_tree = FileTree.from_string(tree.to_string())
    assert len(list(new_tree.iter_vars(["subject", "session"]))) == 3
    for idx, sub_tree in enumerate(new_tree.iter_vars(["subject", "session"])):
        assert sub_tree.placeholders['subject'] == vars[0][idx]
        assert int(sub_tree.placeholders['session']) == vars[1][idx]

    tree.placeholders.unlink('subject', 'session')
    assert len(list(tree.iter_vars(["subject", "session"]))) == 9
    assert tree.placeholders['subject'] == vars[0]
    assert tree.placeholders['session'] == vars[1]

    # test creating sub-tree with linked variables
    tree.placeholders.link('subject', 'session')
    new_tree = FileTree.empty()
    new_tree.add_subtree(tree, precursor=['test'])
    assert new_tree.placeholders['test/subject'] == vars[0]
    assert new_tree.placeholders['test/session'] == vars[1]
    assert len(list(tree.iter_vars(["subject", "session"]))) == 3


def test_get():
    for return_path, return_type in [(False, str), (True, Path)]:
        tree = FileTree.read(directory / "base.tree", subject="A", return_path=return_path)
        assert tree.get("file1") == return_type("A/file1.txt")
        assert tree.get("file2") == return_type("A/01_file2.txt")

        allp = tree.get_mult("file2")
        assert allp.shape == ()
        assert allp.data[()] == return_type("A/01_file2.txt")

        tree2 = tree.update(session=None)
        assert tree2.get("file1") == return_type("A/file1.txt")
        with pytest.raises(KeyError):
            tree2.get("file2")

        tree3 = tree.update(subject=None)
        with pytest.raises(KeyError):
            tree3.get("file1")
        with pytest.raises(KeyError):
            tree3.get("file2")

        tree4 = tree.update(subject=["A", "B"])
        with pytest.raises(KeyError):
            tree4.get("file1")
        with pytest.raises(KeyError):
            tree4.get("file2")

        allp = tree4.get_mult("file2")
        assert allp.shape == (2, )
        assert allp.sel(subject="A") == return_type("A/01_file2.txt")
        assert allp.sel(subject="B") == return_type("B/01_file2.txt")

        tree5 = tree.update(subject=["A", "B"], session=(1, 2))
        allp = tree5.get_mult(('file1', 'file2'))
        assert allp['file1'].shape == (2, )
        assert allp['file2'].shape == (2, 2)
        assert allp.sel(subject='A')['file1'] == return_type("A/file1.txt")
        assert allp.sel(subject='A')['file2'].shape == (2, )
        assert allp.sel(subject='A', session=2)['file2'] == return_type("A/2_file2.txt")


def test_get_mult_child_placholder():
    """Tests a bug in the names used for the xarray dimensions when using sub-trees"""
    tree = FileTree.from_string("sub-file-{sub/place}.txt (sub/file)", place=('A', 'B', 'C'))
    res = tree.get_mult("sub/file")
    assert res.shape == (3, )
    assert res.dims[0] == 'place'
    assert res.name == 'sub/file'
    assert res.sel(place='B').values[()] == "sub-file-B.txt"

    tree.placeholders['sub/place'] = ('D', 'E')
    res = tree.get_mult("sub/file")
    assert res.shape == (2, )
    assert res.dims[0] == 'sub/place'
    assert res.name == 'sub/file'
    assert res.sel({'sub/place': 'E'}).values[()] == "sub-file-E.txt"


def test_iteration():
    tree = FileTree.read(directory / "base.tree", subject=("A", "B", "C"), session=(1, 2))
    allp = tree.get_mult("file2")
    assert allp.size == 6
    assert allp.sel(subject="B", session=1) == "B/1_file2.txt"

    assert len(list(tree.iter("file1"))) == 3
    all_subjects = set()
    for stree in tree.iter("file1"):
        subject = stree.placeholders['subject']
        all_subjects.add(subject)
        assert stree.get("file1") == f"{subject}/file1.txt"
        assert stree.get_mult("file2").shape == (2, )
    assert tuple(sorted(all_subjects)) == ("A", "B", "C")

    assert len(list(tree.iter("file2"))) == 6
    all_subjects = set()
    all_sessions = set()
    for stree in tree.iter("file2"):
        subject = stree.placeholders['subject']
        all_subjects.add(subject)
        session = stree.placeholders['session']
        all_sessions.add(session)
        assert stree.get("file1") == f"{subject}/file1.txt"
        assert stree.get("file2") == f"{subject}/{session}_file2.txt"
    assert tuple(sorted(all_subjects)) == ("A", "B", "C")
    assert tuple(sorted(all_sessions)) == (1, 2)

    # tests fixed bug where iteration fails if both linked and unlinked variables are iterated over
    tree = file_tree.FileTree.from_string("""{As}_{Bs} (fn)
As=1,2
Bs=3,4,5
Cs=6,7""")
    tree.placeholders.link("As", "Cs")
    for stree in tree.iter_vars(["As", "Bs"]):
        assert int(stree.placeholders['As']) in (1, 2)
        assert int(stree.placeholders['Bs']) in (3, 4, 5)
        assert int(stree.placeholders['Cs']) in (6, 7)

def test_io():
    for tree_name in ('base', 'parent', 'multi_key'):
        for top_level in (".", "/data"):
            tree = FileTree.read(directory / f"{tree_name}.tree", top_level=top_level, subject=("A", "B", "C"), session=(1, 2), my_param=(None, 1.23))
            new_tree = parse_tree.read_file_tree_text(tree.to_string().splitlines(), Template(None, top_level))
            assert tree.to_string() == new_tree.to_string()
            for key in tree._templates:
                assert tree.get_template(key).as_path == new_tree.get_template(key).as_path
                for key2 in tree._templates:
                    if tree.get_template(key) is tree.get_template(key2):
                        assert new_tree.get_template(key) is new_tree.get_template(key2)
                    else:
                        assert new_tree.get_template(key) is not new_tree.get_template(key2)
            for key in tree.placeholders:
                if key in ["subject", "session"]:
                    assert [str(elem) for elem in tree.placeholders[key]] == list(new_tree.placeholders[key])
                elif key == "my_param":
                    assert list(new_tree.placeholders["my_param"]) == [None, "1.23"]
                else:
                    assert str(tree.placeholders[key]) == new_tree.placeholders[key]


def test_multi_key():
    tree = FileTree.read(directory / "multi_key.tree")
    assert tree.get_template("file") is tree.get_template("top_level")
    assert tree.get_template("sub1/") is tree.get_template("sub2/")
    assert tree.get_template("sub1/file1") is not tree.get_template("sub2/file1")


@mock.patch("file_tree.template.glob")
def test_glob(mock_glob):
    mock_glob.return_value = [
        "sub-A/ses-A/text.txt",
        "sub-A/ses-B/text.txt",
        "sub-B/text.txt",
        "sub-B/other_text.txt",
        "sub-C/ses-D/sub-session-E/text.txt",  # invalid directory structure; should not affect the results
    ]

    tree = FileTree.from_string("""
    sub-{A}/[ses-{B}]/{C}.txt (all)
    sub-A/ses-{B}/{C}.txt (subA)
    sub-B/{C}.txt (subB)
    """)
    placholders = tree.update_glob("subB").placeholders
    assert len(placholders) == 1
    assert placholders['C'] == ['other_text', 'text']

    placholders = tree.update_glob("subA").placeholders
    assert len(placholders) == 2
    assert placholders['B'] == ['A', 'B']
    assert placholders['C'] == ['text']

    placholders = tree.update_glob("all").placeholders
    assert len(placholders) == 3
    assert placholders['A'] == ['A', 'B']
    assert placholders['B'] == [None, 'A', 'B']
    assert placholders['C'] == ['other_text', 'text']

    placholders = tree.update_glob(("subA", "all")).placeholders
    assert len(placholders) == 3
    assert placholders['A'] == ['A', 'B']
    assert placholders['B'] == [None, 'A', 'B']
    assert placholders['C'] == ['other_text', 'text']

    placholders = tree.update_glob(("subA", "subB")).placeholders
    assert len(placholders) == 2
    assert placholders['B'] == ['A', 'B']
    assert placholders['C'] == ['other_text', 'text']

    for filter in (False, True):
        da = tree.update_glob("subA").get_mult("subB", filter=filter)
        assert da.shape == (1, )
        assert da.sel(C="text").data[()] == 'sub-B/text.txt'
        assert da.sel(C="text").shape == ()

        da = tree.update_glob("all").get_mult("subA", filter=filter)
        assert da.ndim == 2
        assert da.sel(B="A", C="text").data[()] == 'sub-A/ses-A/text.txt'
        assert da.sel(B="A", C="text").shape == ()
        if filter:
            assert da.sel(B="A", C="other_text").data[()] == ""
            assert da.sel(B="A", C="other_text").shape == ()
        else:
            assert da.sel(B="A", C="other_text").data[()] == 'sub-A/ses-A/other_text.txt'
            assert da.sel(B="A", C="other_text").shape == ()

    for template in tree.template_keys():
        assert (tree.update_glob(template).get_mult(template, filter=True) == tree.get_mult_glob(template)).data.all()
    da1 = tree.update_glob(tree.template_keys()).get_mult(tree.template_keys(), filter=True)
    da2 = tree.get_mult_glob(tree.template_keys())
    for key in tree.template_keys():
        assert (da1[key] == da2[key]).data.all()


def test_mult_linked():
    tree = FileTree.from_string("""
{A}.txt (single)
{A}_{B}.txt (mult)
""")
    tree.placeholders[("A", "B")] = [(1, 2, 3), ('a', 'b', 'c')]
    xa = tree.get_mult("single")
    assert xa.shape == (3, )
    assert xa.sel(A=1).data[()] == "1.txt"
    # I would like the following to be true, but it is not:
    #assert xa.sel(A=1).shape == ()

    xa = tree.get_mult("mult")
    assert xa.shape == (3, )
    assert xa.sel(A=2).data[()] == "2_b.txt"
    assert xa.sel(B="b").data[()] == "2_b.txt"
    assert xa.sel(A=1).shape == (1, )
    assert xa.sel(B="b").shape == (1, )