file-tree package
=================

.. automodule:: file_tree
   :members:
   :undoc-members:
   :show-inheritance:

file\_tree.file\_tree module
----------------------------

.. automodule:: file_tree.file_tree
   :members:
   :undoc-members:
   :show-inheritance:

file\_tree.parse\_tree module
-----------------------------

.. automodule:: file_tree.parse_tree
   :members:
   :undoc-members:
   :show-inheritance:

file\_tree.template module
--------------------------

.. automodule:: file_tree.template
   :members:
   :undoc-members:
   :show-inheritance: