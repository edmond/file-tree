.. file-tree documentation master file, created by
   sphinx-quickstart on Fri Jan 29 16:46:10 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to file-tree's documentation!
=====================================
This is the documentation for file-tree, a project designed to allow you to separate the directory structure of your pipeline inputs/outputs from the actual pipeline code.

Install using
::

   pip install file-tree file-tree-fsl

Include the `file-tree-fsl` package if you want to use filetree definitions for the FSL tools.

.. toctree::
   :maxdepth: 3

   tutorial
   changes
   file_tree